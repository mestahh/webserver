package org.mestahh.webserver.io;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class HttpFileReaderTest {

	private HttpFileReader testObj;

	@Before
	public void setUp() {
		testObj = new HttpFileReader();
	}

	@Test
	public void reads_a_text_file_in_and_returns_its_content()
			throws IOException {
		assertEquals("test content\r\n2nd line\r\n",
				testObj.readFile("src/test/resources/www/testText.html"));
	}

}
