package org.mestahh.webserver.io;

import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ReaderFactoryTest {

	private ReaderFactory readerFactory;

	@Mock
	private InputStream inputStreamMock;

	@Before
	public void setUp() {
		readerFactory = new ReaderFactory();
	}

	@Test
	public void creates_a_new_BufferedReader_instance_from_the_input_stream() {
		final BufferedReader bufferedReader = readerFactory
				.createBufferedReader(inputStreamMock);
		assertNotNull(bufferedReader);
	}

	@Test(expected = IllegalArgumentException.class)
	public void throws_exception_if_the_input_stream_is_null() {
		readerFactory.createBufferedReader(null);
	}

	@Test
	public void creates_a_http_file_reader_instance() {
		final HttpFileReader reader = readerFactory.createHttpFileReader();
		assertNotNull(reader);
	}
}
