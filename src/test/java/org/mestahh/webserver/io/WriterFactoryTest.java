package org.mestahh.webserver.io;

import static org.junit.Assert.assertNotNull;

import java.io.OutputStream;
import java.io.PrintWriter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class WriterFactoryTest {

	private WriterFactory writerFactory;

	@Mock
	private OutputStream outputStreamMock;

	@Before
	public void setUp() {
		writerFactory = new WriterFactory();
	}

	@Test
	public void creates_a_new_PrintWriter_instance_from_the_input_stream() {
		final PrintWriter printWriter = writerFactory
				.createPrintWriter(outputStreamMock);
		assertNotNull(printWriter);
	}

	@Test(expected = IllegalArgumentException.class)
	public void throws_exception_if_the_output_stream_is_null() {
		writerFactory.createPrintWriter(null);
	}
}
