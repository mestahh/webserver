package org.mestahh.webserver;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mestahh.webserver.io.HttpFileReader;
import org.mestahh.webserver.io.ReaderFactory;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RequestHandlerTest {

	private RequestHandler testObj;
	@Mock
	private ReaderFactory readerFactoryMock;
	@Mock
	private PrintWriter outputMock;
	@Mock
	private BufferedReader inputMock;
	@Mock
	private HttpFileReader fileReaderMock;
	@Mock
	private Properties propertiesMock;

	@Before
	public void setUp() {
		testObj = new RequestHandler(readerFactoryMock, propertiesMock);
	}

	@Test
	public void retrieves_the_requested_text_file_name_from_the_input_stream_reads_the_file_and_returns_it_as_a_string()
			throws IOException {
		final String request = "GET /test.html HTTP1.1";
		when(inputMock.readLine()).thenReturn(request);
		when(readerFactoryMock.createHttpFileReader()).thenReturn(
				fileReaderMock);
		final String content = "text file test content";
		final String rootDir = "src/test/resources/www";
		when(fileReaderMock.readFile(rootDir + "/test.html")).thenReturn(
				content);
		when(propertiesMock.getProperty("rootDir")).thenReturn(rootDir);

		testObj.handleRequest(inputMock, outputMock);

		verify(propertiesMock).getProperty("rootDir");
		verify(inputMock).readLine();
		verify(readerFactoryMock).createHttpFileReader();
		verify(fileReaderMock).readFile(rootDir + "/test.html");
		verify(outputMock).println(content);
		verify(inputMock).close();
		verify(outputMock).close();
		verify(outputMock).flush();
	}

	@Test
	public void returns_404_Not_Found_if_the_requested_page_is_not_found()
			throws IOException {
		final String request = "GET /test.html HTTP1.1";
		when(inputMock.readLine()).thenReturn(request);
		when(readerFactoryMock.createHttpFileReader()).thenReturn(
				fileReaderMock);

		final String rootDir = "src/test/resources/www";
		when(propertiesMock.getProperty("rootDir")).thenReturn(rootDir);
		doThrow(new FileNotFoundException()).when(fileReaderMock).readFile(
				rootDir + "/test.html");

		testObj.handleRequest(inputMock, outputMock);
		verify(outputMock).println("404 Not Found");

	}

}
