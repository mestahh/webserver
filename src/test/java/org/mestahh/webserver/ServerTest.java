package org.mestahh.webserver;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mestahh.webserver.io.ReaderFactory;
import org.mestahh.webserver.io.WriterFactory;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ServerTest {

	private Server testObj;
	@Mock
	private ServerSocket socket;
	@Mock
	private Socket acceptedSocketMock;
	@Mock
	private InputStream inputStreamMock;
	@Mock
	private OutputStream outputStreamMock;
	@Mock
	private ReaderFactory readerFactoryMock;
	@Mock
	private BufferedReader bufferedReaderMock;
	@Mock
	private PrintWriter printWriterMock;
	@Mock
	private WriterFactory writerFactoryMock;
	@Mock
	private RequestHandler requestHandler;
	@Mock
	private RequestHandler requestHandlerMock;

	@Before
	public void setUp() {
		testObj = new Server(socket, readerFactoryMock, writerFactoryMock,
				requestHandlerMock);
	}

	@Test
	public void server_can_be_stopped() throws IOException {
		testObj.stopServer();
		verify(socket).close();
	}

	@Ignore
	@Test
	public void server_starts_and_waits_for_connections() throws IOException {
		when(socket.accept()).thenReturn(acceptedSocketMock);
		when(acceptedSocketMock.getInputStream()).thenReturn(inputStreamMock);
		when(readerFactoryMock.createBufferedReader(inputStreamMock))
				.thenReturn(bufferedReaderMock);
		when(acceptedSocketMock.getOutputStream()).thenReturn(outputStreamMock);
		when(writerFactoryMock.createPrintWriter(outputStreamMock)).thenReturn(
				printWriterMock);

		testObj.startServer();
		// testObj.stopServer();

		verify(requestHandler).handleRequest(bufferedReaderMock,
				printWriterMock);
	}

}
