package org.mestahh.webserver;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.mestahh.webserver.io.HttpFileReader;
import org.mestahh.webserver.io.ReaderFactory;

public class RequestHandler {

	private final static Logger logger = Logger.getLogger(RequestHandler.class
			.getName());
	private final ReaderFactory readerFactory;
	private final Properties properties;

	public RequestHandler(final ReaderFactory readerFactory,
			final Properties properties) {
		this.readerFactory = readerFactory;
		this.properties = properties;
	}

	public void handleRequest(final BufferedReader input,
			final PrintWriter output) throws IOException {

		final String inputLine = input.readLine();
		logger.log(Level.INFO, "Client request received.");

		String filePath = inputLine.split("\\s")[1].substring(1);
		final HttpFileReader fileReader = readerFactory.createHttpFileReader();
		String content;
		filePath = properties.getProperty("rootDir") + "/" + filePath;

		try {
			content = fileReader.readFile(filePath);
		} catch (final FileNotFoundException e) {
			logger.severe(filePath + " was not found on the server.");
			content = "404 Not Found";
		}
		output.println(content);

		logger.log(Level.INFO, "Response sent.");
		output.flush();
		input.close();
		output.close();
	}

}
