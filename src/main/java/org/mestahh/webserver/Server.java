package org.mestahh.webserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.mestahh.webserver.io.ReaderFactory;
import org.mestahh.webserver.io.WriterFactory;

public class Server extends Thread implements IServer {

	private static final Logger logger = Logger.getLogger(Server.class
			.getName());
	private final ServerSocket socket;
	private final ReaderFactory readerFactory;
	private final WriterFactory writerFactory;
	private final RequestHandler requestHandler;

	public Server(final ServerSocket socket, final ReaderFactory readerFactory,
			final WriterFactory writerFactory,
			final RequestHandler requestHandler) {
		this.socket = socket;
		this.readerFactory = readerFactory;
		this.writerFactory = writerFactory;
		this.requestHandler = requestHandler;

	}

	@Override
	public void startServer() throws IOException {
		logger.log(Level.INFO,
				"Starting webserver on port: " + socket.getLocalPort());

		while (true) {
			final Socket connectionsSocket = socket.accept();
			final InputStream inputStream = connectionsSocket.getInputStream();

			final BufferedReader input = readerFactory
					.createBufferedReader(inputStream);
			final OutputStream outputStream = connectionsSocket
					.getOutputStream();
			final PrintWriter output = writerFactory
					.createPrintWriter(outputStream);

			requestHandler.handleRequest(input, output);

		}
	}

	@Override
	public void stopServer() throws IOException {
		socket.close();
		logger.log(Level.INFO, "Server socket closed.");
	}

}
