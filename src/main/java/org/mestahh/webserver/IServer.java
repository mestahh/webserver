package org.mestahh.webserver;

import java.io.IOException;

public interface IServer {

	public void startServer() throws IOException;

	public void stopServer() throws IOException;

}
