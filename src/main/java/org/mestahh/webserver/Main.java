package org.mestahh.webserver;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.mestahh.webserver.io.ReaderFactory;
import org.mestahh.webserver.io.WriterFactory;

public class Main {

	private final static Logger logger = Logger.getLogger(Main.class.getName());

	public static void main(final String[] args) {
		ServerSocket socket;

		try {
			socket = new ServerSocket(3000);
			final ReaderFactory readerFactory = new ReaderFactory();
			final WriterFactory writerFactory = new WriterFactory();

			final Properties properties = new Properties();
			final FileInputStream in = new FileInputStream(
					"src/main/resources/application.properties");
			properties.load(in);
			in.close();

			final RequestHandler requestHandler = new RequestHandler(
					readerFactory, properties);

			final Server server = new Server(socket, readerFactory,
					writerFactory, requestHandler);
			server.startServer();
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "An error occured. " + e.getMessage());

		}
	}

}