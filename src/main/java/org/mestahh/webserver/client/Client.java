package org.mestahh.webserver.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {

	private static final Logger logger = Logger.getLogger(Client.class
			.getName());

	public static void main(final String[] args) {
		Socket socket;
		final String host = "localhost";
		try {
			socket = new Socket(host, 3000);
			logger.log(Level.INFO,
					"Connection estabelished with " + socket.getInetAddress());

			final OutputStream outstream = socket.getOutputStream();
			final PrintWriter writer = new PrintWriter(outstream);

			// final Scanner scanner = new Scanner(System.in);
			// final String request = scanner.nextLine();

			final BufferedReader inputStream = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));

			if (socket != null && writer != null && inputStream != null) {

				writer.println("GET /akari.htm HTTP1.1");
				writer.flush();
				String responseLine;

				while ((responseLine = inputStream.readLine()) != null) {
					logger.log(Level.INFO, "Server response: " + responseLine);
					if (responseLine.indexOf("Ok") != -1) {
						break;
					}
				}
			}

			outstream.close();
			inputStream.close();

		} catch (final UnknownHostException e) {
			logger.log(Level.SEVERE, "Host (" + host + " is not available.");
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "An I/O error occured. " + e.getMessage());
		}

	}
}
