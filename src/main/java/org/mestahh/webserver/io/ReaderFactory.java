package org.mestahh.webserver.io;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReaderFactory {

	private static final Logger logger = Logger.getLogger(ReaderFactory.class
			.getName());

	public BufferedReader createBufferedReader(final InputStream inputStream)
			throws IllegalArgumentException {
		if (inputStream == null) {
			logger.log(Level.SEVERE, "The input stream was null!");
			throw new IllegalArgumentException("The input stream was null!");
		}
		return new BufferedReader(new InputStreamReader(inputStream));
	}

	public HttpFileReader createHttpFileReader() {
		return new HttpFileReader();
	}
}
