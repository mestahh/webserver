package org.mestahh.webserver.io;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WriterFactory {

	private static final Logger logger = Logger.getLogger(ReaderFactory.class
			.getName());

	public PrintWriter createPrintWriter(final OutputStream outputStream)
			throws IllegalArgumentException {
		if (outputStream == null) {
			logger.log(Level.SEVERE, "The output stream was null!");
			throw new IllegalArgumentException("The output stream was null!");
		}
		return new PrintWriter(outputStream);
	}
}
