package org.mestahh.webserver.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class HttpFileReader {

	public String readFile(final String filePath) throws IOException {

		final FileReader fileReader = new FileReader(filePath);
		final BufferedReader reader = new BufferedReader(fileReader);
		final StringBuffer content = new StringBuffer();

		String line;
		while ((line = reader.readLine()) != null) {
			content.append(line + "\r\n");
		}
		reader.close();
		return content.toString();
	}
}
